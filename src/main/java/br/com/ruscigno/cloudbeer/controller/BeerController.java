package br.com.ruscigno.cloudbeer.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ruscigno.cloudbeer.model.Beer;

@Controller
public class BeerController {

	@RequestMapping("/beer/add")
	public String add() {
		return "beer/BeerRegister";
	}

	@RequestMapping(value="/beer/add", method = RequestMethod.POST)
	public String register(@Valid Beer beer, BindingResult result){
		if(result.hasErrors()){
			System.out.println("tem erro");
		}

		System.out.println("adding... " + beer.getSku() + "," + beer.getName());
		return "beer/BeerRegister";
	}
}
